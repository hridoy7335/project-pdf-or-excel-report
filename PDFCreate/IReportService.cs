﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFCreate
{
    public interface IReportService
    {
        public byte[] GeneratePdfReport(string page);
    }
}
