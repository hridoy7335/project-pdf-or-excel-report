﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;


namespace PDFCreate.Service
{
    public class DemoEmailService : IEmailService
    {
        public async Task SendAsync(string email, string name, string subject, string body)
        {
            using (var smtp = new SmtpClient())
            {
                smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                smtp.PickupDirectoryLocation = @"c:\maildump";
                var message = new MailMessage
                {
                    Body = body,
                    Subject = subject,
                    From = new MailAddress(email, name),
                    IsBodyHtml = true
                };
                message.To.Add("contact@domain.com");
                await smtp.SendMailAsync(message);
            }
        }
    }
}
