﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFCreate.Service
{
    public interface IEmailService
    {
        Task SendAsync(string email, string name, string subject, string body);
    }
}
