﻿using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Mvc;
using PDFCreate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFCreate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportService _reportService;
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }
        private List<Author> authors = new List<Author>
        {
            new Author { Id = 1, FirstName = "Joydip", LastName = "Kanjilal" },
            new Author { Id = 2, FirstName = "Steve", LastName = "Smith" },
            new Author { Id = 3, FirstName = "Anand", LastName = "Narayaswamy"}
        };


        [HttpPost]
        public IActionResult Post(HTMLBodyCollector htmlPageObj)
        {

            
            
            var pdfFile = _reportService.GeneratePdfReport(htmlPageObj.htmlPage);
            return File(pdfFile,
            "application/octet-stream", "SimplePdf.pdf");
        }

        private string PartialView(string v)
        {
            throw new NotImplementedException();
        }
    }
}
