﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDFCreate.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFCreate.Controllers
{
    public class HomeController : Controller
    {
       public List<Author> authors = new List<Author>
        {
            new Author { Id = 1, FirstName = "Joydip", LastName = "Kanjilal" },
            new Author { Id = 2, FirstName = "Steve", LastName = "Smith" },
            new Author { Id = 3, FirstName = "Anand", LastName = "Narayaswamy"}
        };


        public IActionResult DownloadCommaSeperatedFile()
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("Id,FirstName,LastName");
                foreach (var author in authors)
                {
                    stringBuilder.AppendLine($"{author.Id}, { author.FirstName},{ author.LastName}");
                }
                return File(Encoding.UTF8.GetBytes
                (stringBuilder.ToString()), "text/csv", "authors.csv");
            }
            catch(Exception ex)
            {
                return View("Index");
            }
        }

        public IActionResult Index()
        {
            return RedirectToAction("Contact", "ContactModel");
            //return RedirectToAction("info");
            // return View();
        }
       
        public IActionResult info()
        {

           // var str= PartialView("_Test");
            return RedirectToAction("Get", "Report");
          //  return View();
      
        }


        public IActionResult Excel()
        {
            var workbook = new XLWorkbook();
            
                IXLWorksheet worksheet = workbook.Worksheets.Add("Authors");
                worksheet.Cell(1, 1).Value = "Id";
                worksheet.Cell(1, 2).Value = "FirstName";
                worksheet.Cell(1, 3).Value = "LastName";
                for (int index = 1; index <= authors.Count; index++)
                {
                    worksheet.Cell(index + 1, 1).Value = authors[index - 1].Id;
                    worksheet.Cell(index + 1, 2).Value = authors[index - 1].FirstName;
                    worksheet.Cell(index + 1, 3).Value = authors[index - 1].LastName;
                }

              
    
            using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);
                var content = stream.ToArray();
                return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "author.xlsx");
            }
        }

        public IActionResult Privacy()
        {

            return View();
        }

    }
}
