﻿using Microsoft.AspNetCore.Mvc;
using PDFCreate.Models;
using PDFCreate.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFCreate.Controllers
{
    public class ContactModelController : Controller
    { 
        private readonly IRazorPartialToStringRenderer _renderer;
        private readonly IEmailService _emailer;
        private readonly IReportService _reportService;
        public ContactModelController(IRazorPartialToStringRenderer renderer, IEmailService emailer, IReportService reportService)
        {
            _renderer = renderer;
            _emailer = emailer;
            _reportService = reportService;
        }
        //[BindProperty]
        //public ContactForm ContactForm { get; set; }
        [TempData]
        public string PostResult { get; set; }
        public async Task<IActionResult> Contact()
        {
            ContactForm contactForm = new ContactForm();
            contactForm.Id = 1;
            contactForm.Name = "Roy";
            contactForm.Message = "Roy 12345";
            contactForm.Email = "Roy@12345";


            var body = await _renderer.RenderPartialToStringAsync("_ContactEmailPartial", contactForm);
            //await _emailer.SendAsync(ContactForm.Name, ContactForm.Email, ContactForm.Subject, body);
            PostResult = "Check your specified pickup directory";
            //return RedirectToPage(""); 
            HTMLBodyCollector htmlPageObj = new HTMLBodyCollector();
            htmlPageObj.htmlPage = body;
            //return RedirectToAction("Post", "Report", htmlPageObj);
            var pdfFile = _reportService.GeneratePdfReport(htmlPageObj.htmlPage);
            return File(pdfFile,
            "application/octet-stream", "SimplePdf.pdf");
        }

     
    } 
   
}

