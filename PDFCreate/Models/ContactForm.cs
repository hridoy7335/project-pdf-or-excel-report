﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PDFCreate.Models
{
    public class ContactForm
    {
        public int Id { get; set; } 
        public string Email { get; set; }
        public string Message { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public Priority Priority { get; set; }
    }
    public enum Priority
    {
        Low, Medium, High
    }
}
